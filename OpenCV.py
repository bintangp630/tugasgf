import cv2 as c
import pyzbar.pyzbar as p

cap = c.VideoCapture("123.mp4")

while(True):
    ret, frame = cap.read()

    decodeObjects = p.decode(frame)
    for obj in decodeObjects:
        (x,y,w,h) = obj.rect
        c.rectangle(frame,(x,y), (x + w, y + h), (0, 255, 0),2)
        print("Tipe: ", obj.type)
        print("Data: ", obj.data, "\n")
        c.putText(frame, str(obj.data), (150,480), c.FONT_HERSHEY_COMPLEX, 2.5, (0, 255, 0), 2)

    c.imshow("Frame", frame)

    if c.waitKey(16) == 27:
        break

